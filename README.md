# README #

* Quick summary

This project is a Chat made in Angular Node and Socket. Initially, this project was meant to be hosted on Heroku but Socket.io is not compatible.

I will use Websockets (npm search ws) to in order to showcase the Combo Angular/Node/Heroku and Realtime communication.

ACHTUNG / ATTENTION / BEWARE

Some SSL configurations can block to connection with the MongoLab NetWork.


* Version 0.0.1


### How do I get set up? ###

* Summary of set up

 git clone ... && cd ...
 npm install
 cd public && bower install
 npm start

* Dependencies

- express
- fs
- body-parser
- bower
- angular
- mongojs
- path
- lodash
- socket.io  ... More information in package.json
