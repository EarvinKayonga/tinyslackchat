(function() {
  'use strict';
  var ChatApp = angular.module('ChatApp', []);

  ChatApp.controller('mainCtrl', function($scope, $http) {
    // Retrieve content on the initial connection which is stored in MongoDb
    $scope.chatObjs = [];
    $http.get('/chat').then(function(res) {
      if (res) {
        $scope.chatObjs = res.data;
      }
    });

    $scope.randomColor = function() {
      var letters = '0123456789ABCDEF'.split('');
      var color = '#';
      for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
      }
      return color;
    };

    $scope.send = function() {
      if ($scope.toSend.length) {
        window.message = $scope.toSend;
        var toDisplay = "<div class=ligne><h4 class= pseudo > " + window.pseudo+"@chatbyearvin" + " </h4> : <h4 class=message>" + $scope.toSend + "</h4></div>";
        angular.element('.chat').append(toDisplay);
        $scope.toSend = "";
      }
    }
  });

})();
