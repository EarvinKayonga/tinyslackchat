(function() {
  'use strict';

  // Config

  var express = require('express'),
    app = express(),

    _ = require('lodash'),
    Q = require('q'),
    path = require('path'),
    bodyParser = require('body-parser'),
    mongojs = require('mongojs'),
    fs = require('fs'),
    less = require('less'),
    http = require("http").createServer(app),
    io = require('socket.io').listen(http),


    config = require(path.resolve('./config/config'))(),
    db = mongojs(config.db.address, [config.db.collection.address]).heroku_l9qg0fsh,
    port = process.env.PORT || 10002;

  http.listen(port, "localhost");
  console.log("Server running on " + port);


  app.use(express.static(__dirname + "/public")); // enable file server features for the public folder
  app.use(bodyParser.json());


  // Socket.io Section.

  io.sockets.on('connection', function(socket, pseudo) {

    socket.on('new', function(pseudo) {
      socket.pseudo = pseudo;
    });

    socket.on('message', function(message) {
      var toSend = {
        "message": message,
        "pseudo": socket.pseudo || "unknown"
      };
      socket.broadcast.emit('message', toSend);

      db.insert(toSend, function(err, doc) {
      });

    });
  });


  // Routing section

  app.get('*.less', function(req, res) {
    var path = __dirname + req.url;
    fs.readFile(path, "utf8", function(err, data) {
      if (err) throw err;
      less.render(data, function(err, css) {
        if (err) throw err;
        res.header("Content-type", "text/css");
        res.send(css.css);
      });
    });
  });


  app.get('/chat', function(req, res) {
    db.find(function(err, docs) {
      res.json(docs);
    });
  });






})();
